#include "config.hpp"
#include <fstream>

PHASE_SCAN::~PHASE_SCAN(){}
PHASE_SCAN::PHASE_SCAN(LOGGER& _MESSENGER_, const std::pair<const std::string&, std::vector<double>>& _PARAMETERS_)
{
    _MESSENGER_.SendInfo("Initialized a Phase Scan.");
    _MESSENGER_.SendInfo("file_name was read to be: " + _PARAMETERS_.first);
}
void PHASE_SCAN::SetThreshold(const double& new_threshold_) {_threshold = new_threshold_;}

TORNADO::TORNADO(){}
TORNADO::~TORNADO(){}
void TORNADO::SetThreshold(const double& new_threshold_) {_threshold = new_threshold_;}

CHARGE_SCAN::CHARGE_SCAN(){}
CHARGE_SCAN::~CHARGE_SCAN(){}
void CHARGE_SCAN::SetThreshold(const double& new_threshold_) {_threshold = new_threshold_;}

CONFIG::CONFIG(const std::string& _CONFIG_PATH_)
{
    std::ifstream CONFIG_FILE;
  	CONFIG_FILE.open(_CONFIG_PATH_.c_str());

    if (CONFIG_FILE.is_open())
	{
        std::string line;

        while (std::getline(CONFIG_FILE, line))
		{
			if (line.find("[TORNADO]") != std::string::npos)
            {
                std::unique_ptr<TORNADO> _NEW_TORNADO_;
                _CURRENT_TORNADO_SCAN_ = _NEW_TORNADO_;
                is_Reading = 1; CURRENT_SCAN = "TORNADO";
            }
            
            if (line.find("THRESHOLD") != std::string::npos)
				{
					size_t D_Letter = line.find('D');
					double THRESHOLD = std::stod(line.substr(D_Letter + 4));
                    if (is_Reading && CURRENT_SCAN == "TORNADO") _CURRENT_TORNADO_SCAN_->SetThreshold(THRESHOLD);
                    else if (is_Reading && CURRENT_SCAN == "CHARGE SCAN") _CURRENT_CHARGE_SCAN_->SetThreshold(THRESHOLD);
                    else if (is_Reading && CURRENT_SCAN == "PHASE SCAN") _CURRENT_PHASE_SCAN_->SetThreshold(THRESHOLD);
                    else 
				}
        }

    }
}