#ifndef OBJECTS_H
#define OBJECTS_H

#include <vector>
#include <cmath>
#include <string>
#include <fstream>
#include <iostream>

#define PI 3.14159265
#define CLK_CYCLE 25.0

using Range = std::vector<double>;

class CLOCK
{
    public:
        CLOCK(Range DOMAIN);
        CLOCK();
        ~CLOCK();
        std::vector<unsigned long int> GetRisingEdges();
        std::vector<unsigned long int> GetFallingEdges();
        Range GetSignal();
        double GetPhase();
    private:
        std::vector<unsigned long int> RIS_EDGES;
        std::vector<unsigned long int> FAL_EDGES;
        Range SIGNAL;
        double OFFSET;
        friend class EVENT;
};

class COMP_OUT
{
    public:
        COMP_OUT(double _DURATION_, double _threshold, double _offset, Range DOMAIN);
        COMP_OUT();
        ~COMP_OUT();
        Range GetSignal();
        double GetDuration();
    private:
        Range SIGNAL;
        double _DURATION;
        double _CHARGE;
        double _TIME_WALK;
        double TIME_WALK(double, double);
        double DURATION(double, double);
        friend class EVENT;
};

class EVENT
{
    public:
        EVENT(double, double, double);
        ~EVENT();
        unsigned short int GetToT();
        void SaveEvent(const std::string& _save = "example");
    private:
        double DURATION(double charge, double _threshold);
        double TIME_WALK(double charge, double _threshold);
        double TRIGGER;
        signed short int ToT;
        double THRESHOLD;
        double CHARGE_INJECTION;
        double CLK_OFFSET;
        Range LIN_INTERVAL(double _start_ = 0., double _stop_ = 100, unsigned long int num = 1e4);
        Range TIME_DOMAIN;
        CLOCK _CLK_;
        COMP_OUT _COMP_OUT_;
};

#endif