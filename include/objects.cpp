#include "objects.hpp"
#include <filesystem>

/**
 * DEFINITIONS FOR THE CLOCK CLASS INSIDE EVENT 
 */

CLOCK::CLOCK(Range DOMAIN) 
{
    SIGNAL = Range(DOMAIN.size(), 0);
    double previous;
    for (unsigned long int index = 0; index < DOMAIN.size(); ++index) 
    {
        if (index == 0)
        { 
            SIGNAL[index] = (sin((DOMAIN[index]) * 2 * PI / CLK_CYCLE) > 0) ? 1 : 0;
            previous = SIGNAL[0];
        }
        else
        {
            SIGNAL[index] = (sin((DOMAIN[index]) * 2 * PI / CLK_CYCLE) > 0) ? 1 : 0;
            previous = SIGNAL[index - 1];
            if (previous == 0 && SIGNAL[index] == 1) {RIS_EDGES.push_back(index);}
            else if (previous == 1 && SIGNAL[index] == 0) {FAL_EDGES.push_back(index - 1);}
        }

    }
}
CLOCK::~CLOCK(){}
CLOCK::CLOCK(){}
std::vector<unsigned long int> CLOCK::GetRisingEdges() {return RIS_EDGES;}
std::vector<unsigned long int> CLOCK::GetFallingEdges() {return FAL_EDGES;}
Range CLOCK::GetSignal() {return SIGNAL;}

/**
 * DEFINITIONS FOR THE COMP_OUT CLASS INSIDE EVENT 
 */

COMP_OUT::COMP_OUT(double _charge, double _threshold, double _offset, Range DOMAIN) 
{
    _DURATION = DURATION(_charge, _threshold);
    _TIME_WALK = TIME_WALK(_charge, _threshold);
    _CHARGE = _charge;
    SIGNAL = Range(DOMAIN.size(), 0);
    for (unsigned long int index = 0; index < SIGNAL.size(); ++index)
    {
        SIGNAL[index] = (DOMAIN[index] < _DURATION + _TIME_WALK + _offset && DOMAIN[index] >= _TIME_WALK + _offset) ? 1 : 0;
    }
}
COMP_OUT::~COMP_OUT(){}
COMP_OUT::COMP_OUT(){}
double COMP_OUT::GetDuration() {return _DURATION;}
Range COMP_OUT::GetSignal() {return SIGNAL;}
double COMP_OUT::TIME_WALK(double _charge, double _threshold) 
{
    double TIME_WALK = (6.24573346e-04 * _threshold + 7.07441732) * exp(-(_charge - _threshold)/(0.0555666 * _threshold + 24.84357041) ) + (16.177236798564252 * _threshold + 2527.537067765581) / _charge + (-0.00038665573601304583 * _threshold + 14.324674481669573);
    return TIME_WALK;
}
double COMP_OUT::DURATION(double _charge, double _threshold)
{
    double TIME_WALK = (6.24573346e-04 * _threshold + 7.07441732) * exp(-(_charge - _threshold)/(0.0555666 * _threshold + 24.84357041) ) + (16.177236798564252 * _threshold + 2527.537067765581) / _charge + (-0.00038665573601304583 * _threshold + 14.324674481669573);
    double TIME_END = 0.019 * _charge + (-0.020 * _threshold + 32.452);
    return (TIME_END - TIME_WALK);
}

/**
 * DEFINITIONS FOR THE EVENT 
 */

EVENT::EVENT(double _CHARGE, double _CLK_OFFSET_, double _THRESHOLD_)
{
    if (_CHARGE < _THRESHOLD_) {ToT = 15;}
    else
    {
        ToT = -1;
        TIME_DOMAIN = LIN_INTERVAL(- 2 * CLK_CYCLE, _CLK_OFFSET_ + DURATION(_CHARGE, _THRESHOLD_) + TIME_WALK(_CHARGE, _THRESHOLD_) + 2 * CLK_CYCLE, 1e4);
        
        COMP_OUT EVT_COMP(_CHARGE, _THRESHOLD_, _CLK_OFFSET_, TIME_DOMAIN); 
        _COMP_OUT_ = EVT_COMP;
        
        CLOCK EVT_CLOCK(TIME_DOMAIN); 
        _CLK_ = EVT_CLOCK;
        CHARGE_INJECTION = _CHARGE;

        auto CLK_RISING_EDGES = _CLK_.GetRisingEdges();
        if (TIME_WALK(_CHARGE, _THRESHOLD_) + _CLK_OFFSET_ < CLK_CYCLE && TIME_WALK(_CHARGE, _THRESHOLD_) + _CLK_OFFSET_ >= 0)
        {
            for (auto& ris_edge : CLK_RISING_EDGES)
            {
                
                if (_CLK_.GetSignal()[ris_edge] == 1 && _COMP_OUT_.GetSignal()[ris_edge] == 1) {ToT += 1;}
            }
            if (ToT == -1) {ToT = 15;}
            else if (ToT > 14) {ToT = 14;}
        }
        else {ToT = 15;}
    }
}
EVENT::~EVENT(){}
unsigned short int EVENT::GetToT() {return ToT;}
void EVENT::SaveEvent(const std::string& _output_name_)
{
    std::ofstream output; 
    output.open(_output_name_ + "/signal.csv");
    for (unsigned long int index = 0; index < TIME_DOMAIN.size(); ++index)
    {
        output << TIME_DOMAIN[index] << ", " << _CLK_.GetSignal()[index] << ", " << _COMP_OUT_.GetSignal()[index] << std::endl;
    }
    output.close();
    std::ofstream output_1; 
    output_1.open(_output_name_ + "/clock_ris_edges.csv");
    for (auto& ris_edge : _CLK_.GetRisingEdges())
    {
        output_1 << TIME_DOMAIN[ris_edge] << std::endl;
    }
    output_1.close();
    std::ofstream output_2; 
    output_2.open(_output_name_ + "/clock_fal_edges.csv");
    for (auto& fal_edge : _CLK_.GetFallingEdges())
    {
        output_2 << TIME_DOMAIN[fal_edge] << std::endl;
    }
    output_2.close();
    std::ofstream output_3; 
    output_3.open(_output_name_ + "/ToT.csv");
    output_3 << ToT << std::endl;
    output_3.close();
}
Range EVENT::LIN_INTERVAL(double _start_, double _stop_, unsigned long int num)
{
    Range output(num, 0);
    for (unsigned long int index = 0; index < num; index++) 
    {
        output[index] = (_start_ + (_stop_ - _start_) * double(index) / double(num - 1));
    }
    return output;
}
double EVENT::DURATION(double charge, double _threshold)
{
    double TIME_WALK = (6.24573346e-04 * _threshold + 7.07441732) * exp(-(charge - _threshold)/(0.0555666 * _threshold + 24.84357041) ) + (16.177236798564252 * _threshold + 2527.537067765581) / charge + (-0.00038665573601304583 * _threshold + 14.324674481669573);
    double TIME_END = 0.01944 * charge + (-0.02408 * _threshold + 28.3);
    return (TIME_END - TIME_WALK);
}
double EVENT::TIME_WALK(double _charge, double _threshold) 
{
    double TIME_WALK = (6.24573346e-04 * _threshold + 7.07441732) * exp(-(_charge - _threshold)/(0.0555666 * _threshold + 24.84357041) ) + (16.177236798564252 * _threshold + 2527.537067765581) / _charge + (-0.00038665573601304583 * _threshold + 14.324674481669573);
    return TIME_WALK;
}