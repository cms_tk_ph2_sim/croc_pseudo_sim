/**   
 *
 */

#ifndef CONFIG_H
#define CONFIG_H

#include "log.hpp"
#include <vector>
#include <memory>

class PHASE_SCAN
{
    public:
        /**
         * @param _MESSENGER_  -> The class that handles messanges to the viewer.
         * @param _PARAMETERS_ -> The vector that sets the parameters of the phase_scan.
         * @param _PARAMETERS_.first -> sets file_name; the name of the output file.
         * @param _PARAMETERS_.second[0] -> sets _phase_start_
         * @param _PARAMETERS_.second[1] -> sets _phase_stop_
         * @param _PARAMETERS_.second[2] -> sets _phase_step_
         * @param _PARAMETERS_.second[3] -> sets _charge_start_
         * @param _PARAMETERS_.second[4] -> sets _charge_stop_
         * @param _PARAMETERS_.second[5] -> sets _charge_step_
         * @param _PARAMETERS_.second[6] -> sets _threshold_
         * @param _PARAMETERS_.second[7] -> sets _krummenacher_
         */
        PHASE_SCAN(LOGGER& _MESSENGER_, const std::pair<const std::string&, std::vector<double>>& _PARAMETERS_);
        ~PHASE_SCAN();
        void SetThreshold(const double&);
        void Execute();
    private:
        std::string file_name;
        double _phase_start_ = 0;
        double _phase_stop_ = 25;
        double _phase_step_ = 1;
        double _charge_start_ = 0;
        double _charge_stop_ = 3e4;
        double _charge_step_ = +10;
        double _threshold = 1000;
        double _krummenacher = 100;
};

class CHARGE_SCAN
{
    public:
        CHARGE_SCAN();
        ~CHARGE_SCAN();
        void SetThreshold(const double&);
        void Execute();
    private:
        std::string file_name;
        double _phase_start_ = 0;
        double _phase_stop_ = 25;
        double _phase_step_ = 1;
        double _charge_start_ = 0;
        double _charge_stop_ = 3e4;
        double _charge_step_ = +10;
        double _threshold = 1000;
        double _krummenacher = 100;
};

class TORNADO
{
    public:
        TORNADO();
        ~TORNADO();
        void SetVariables();
        void Execute();
        void SetThreshold(const double&);
    private:
        std::string file_name;
        double _phase_start_ = 0;
        double _phase_stop_ = 25;
        double _phase_step_ = 1;
        double _charge_start_ = 0;
        double _charge_stop_ = 3e4;
        double _charge_step_ = +10;
        double _threshold = 1000;
        double _krummenacher = 100;
};

class CONFIG
{
    public:
        CONFIG(const std::string& _PATH_);
        ~CONFIG();
    private:
        PHASE_SCAN* _CURRENT_PHASE_SCAN_;
        CHARGE_SCAN* _CURRENT_CHARGE_SCAN_;
        TORNADO* _CURRENT_TORNADO_SCAN_;
        bool is_Reading;
        std::string CURRENT_SCAN;
};

#endif