#include <cmath>

double TIME_WALK(double _charge, double _threshold, double _krummenacher) 
{
    double TIME_WALK = (6.24573346e-04 * _threshold + 7.07441732) * exp(-(_charge - _threshold)/(0.0555666 * _threshold + 24.84357041) ) + (16.177236798564252 * _threshold + 2527.537067765581) / _charge + (-0.00038665573601304583 * _threshold + 14.324674481669573);
    return TIME_WALK;
}

double DURATION(double _charge, double _threshold, double krummenacher)
{
    double TIME_WALK = (6.24573346e-04 * _threshold + 7.07441732) * exp(-(_charge - _threshold)/(0.0555666 * _threshold + 24.84357041) ) + (16.177236798564252 * _threshold + 2527.537067765581) / _charge + (-0.00038665573601304583 * _threshold + 14.324674481669573);
    double TIME_END = 0.019 * _charge + (-0.020 * _threshold + 32.452);
    return (TIME_END - TIME_WALK);
}