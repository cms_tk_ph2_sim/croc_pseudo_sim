void ToTChargeScan(LOGGER& MESSENGER, double THRESHOLD, double _MAX_, double step = 10, double CLK_OFFSET = 0);
void ToTPhaseScan(double start = 0., double stop = 25., double step = 5.);
void TornadoPlot(LOGGER& MESSENGER, double THRESHOLD, double offset_step = +2.5);