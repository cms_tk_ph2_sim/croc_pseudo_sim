#include "log.hpp"
#include <fstream>
#include "objects.hpp"
#include "utils.hpp"

void ToTChargeScan(LOGGER& MESSENGER, double THRESHOLD, double _MAX_, double step, double CLK_OFFSET)
{
    std::ofstream output;
    output.open("Scan.csv");
    for (double ci = THRESHOLD - 50; ci < _MAX_; ci += step)
    {
        EVENT MEASUREMENT(ci, CLK_OFFSET, THRESHOLD);
        output << CLK_OFFSET << ", " << ci << ", " << MEASUREMENT.GetToT() << std::endl;
    }
    output.close();
}
void ToTPhaseScan(double start, double stop, double step)
{
	std::ofstream output;
    output.open("Scan.csv");
	for (double phase = start; phase < stop; phase += step)
	{
		std::cout << "phase: " << phase << std::endl;
		for (double ci = 0; ci < 3e4; ci += 10)
		{
			EVENT MEASUREMENT(ci, phase, 1e3);
			output << phase << ", " << ci << ", " << MEASUREMENT.GetToT() << std::endl;
		}
	}
	output.close();
}
void TornadoPlot(LOGGER& MESSENGER, double THRESHOLD, double offset_step, double _charge_start_, double _charge_end_, double _charge_step)
{
	std::ofstream tornado;
	tornado.open("tornado_plot.csv");
	for (double offset = -CLK_CYCLE + offset_step; offset < CLK_CYCLE; offset += offset_step)
	{
		MESSENGER.SendInfo("PROGRESS - " + std::to_string(2 * CLK_CYCLE - 1 - offset_step), 1);
		std::cout << '\r' << std::flush;
		for (double charge = _charge_start_; charge < _charge_end_; charge += _charge_step)
		{
			EVENT MEASUREMENT(charge, offset, THRESHOLD);
			tornado << charge << ", " << offset << ", " << MEASUREMENT.GetToT() << std::endl;
		}
	}
	tornado.close();
}