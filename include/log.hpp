#ifndef LOG_H
#define LOG_H
#pragma once

#include <iostream>
#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>

#define _RED_       "\033[1;31m"
#define _YELLOW_     "\033[1;33m"
#define _GREEN_    "\033[1;32m"
#define _BLUE_    "\033[1;34m"
#define _RETURN_    "\033[0;37m"
#define _ARROW_     " ➤➤ "

class LOGGER
{
    public:
        LOGGER();
        ~LOGGER();
        void SendInfo(const std::string&, bool RESET = 0);
        void SendSuccess(const std::string&);
        void SendWarning(const std::string&);
        void SendFatal(const std::string&);
    private:
        std::string COLOR(const std::string, const std::string, const std::string);
        std::string M(const std::string, unsigned long int);
};

#endif