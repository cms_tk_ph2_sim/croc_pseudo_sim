import numpy as np
import matplotlib.pyplot as plt

FILE = np.genfromtxt("./Scan.csv", delimiter = ", ")

plt.plot(FILE[:,1], FILE[:,2], color = 'blue')
plt.xlabel(r"Charge [e]", fontsize = 15)
plt.ylabel(r"ToT Code", fontsize = 15)
plt.show(block = True)
