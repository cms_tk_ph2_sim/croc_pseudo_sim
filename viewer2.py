import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "font.sans-serif": ["Computer Modern Roman"]})

FILE = np.genfromtxt("./Scan.csv", delimiter = ", ")

CLK_PHASES = np.unique(FILE[:,0])
for phase in CLK_PHASES:
    color = np.random.rand(3,)
    plt.plot(FILE[FILE[:,0] == phase, 1], FILE[FILE[:,0] == phase, 2], color = color, label = r"{:.0f} ns".format(phase))

Y_TICKS = np.arange(start = 0, stop = 16, step = 1)
Y_TICKS_STR = []
for i in Y_TICKS: Y_TICKS_STR.append(r"\texttt{" + str(i) + "}")
plt.yticks(ticks = Y_TICKS, labels = Y_TICKS_STR)
plt.legend()
plt.show(block = True)
