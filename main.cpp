#include <objects.hpp>
#include <boost/program_options.hpp>
#include <log.hpp>
#include <utils.hpp>
#include <config.hpp>

namespace po = boost::program_options;

int main(int ac, char *av[])
{
	LOGGER _GLOBAL_LOG_;
	std::string _CONFIG_PATH_;

	boost::program_options::options_description _DESC_("Allowed Options:");
	_DESC_.add_options()
	("help", "produce help message")
	("-c", po::value<std::string>(&_CONFIG_PATH_), "PATH of Configuration File");

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(ac, av, _DESC_), vm);
	boost::program_options::notify(vm);

	if (vm.count("help")) 
	{
			std::cout << _DESC_ << "\n";
			return 1;
	}

	std::ifstream CONFIG_FILE;
  	CONFIG_FILE.open(_CONFIG_PATH_.c_str());
	if (CONFIG_FILE.is_open())
	{
		std::string line;

		std::tuple<bool, bool, bool> TORNADO_STATUS = std::make_tuple(0, 0, 0);
		std::pair<double, double> TORNADO_VARS = std::make_pair(0, 0);

		std::tuple<bool, bool, bool, bool, bool> CS_STATUS = std::make_tuple(0, 0, 0, 0, 0);
		std::tuple<double, double, double, double> CS_VARS = std::make_tuple(0, 0, 0, 0);

		while (std::getline(CONFIG_FILE, line))
		{
			if (line.find("[TORNADO PLOT]") != std::string::npos) std::get<0>(TORNADO_STATUS) = 1;
			if (std::get<0>(TORNADO_STATUS) == 1)
			{
				if (line.find("THRESHOLD") != std::string::npos)
				{
					size_t D_Letter = line.find('D');
					double THRESHOLD = std::stod(line.substr(D_Letter + 4));
					std::get<1>(TORNADO_STATUS) = 1;
					std::get<0>(TORNADO_VARS) = THRESHOLD;
				}
				else if (line.find("TIME STEP") != std::string::npos)
				{
					size_t D_Letter = line.find('P');
					double CHARGE_STEP = std::stod(line.substr(D_Letter + 4));
					std::get<2>(TORNADO_STATUS) = 1;
					std::get<1>(TORNADO_VARS) = CHARGE_STEP;
				}
			}
			if (std::get<0>(TORNADO_STATUS) == 1 && std::get<1>(TORNADO_STATUS) == 1 && std::get<2>(TORNADO_STATUS) == 1)
			{
				_GLOBAL_LOG_.SendInfo("Executing Tornado Plot with THR = " + std::to_string((int)std::get<0>(TORNADO_VARS)) + "e and TS = " + 
							std::to_string(std::get<1>(TORNADO_VARS)) + "ns");
				TornadoPlot(_GLOBAL_LOG_,std::get<0>(TORNADO_VARS), std::get<1>(TORNADO_VARS));
			}

			if (line.find("[CHARGE SCAN]") != std::string::npos) std::get<0>(CS_STATUS) = 1;
			if (std::get<0>(CS_STATUS) == 1)
			{
				if (line.find("THRESHOLD") != std::string::npos)
				{
					size_t D_Letter = line.find('D');
					double THRESHOLD = std::stod(line.substr(D_Letter + 4));
					std::get<1>(CS_STATUS) = 1;
					std::get<0>(CS_VARS) = THRESHOLD;
				}
				else if (line.find("MAX") != std::string::npos)
				{
					size_t D_Letter = line.find('X');
					double MAX = std::stod(line.substr(D_Letter + 4));
					std::get<2>(CS_STATUS) = 1;
					std::get<1>(CS_VARS) = MAX;
				}
				else if (line.find("CHARGE STEP") != std::string::npos)
				{
					size_t D_Letter = line.find('P');
					double CHARGE_STEP = std::stod(line.substr(D_Letter + 4));
					std::get<3>(CS_STATUS) = 1;
					std::get<2>(CS_VARS) = CHARGE_STEP;
				}
				else if (line.find("OFFSET") != std::string::npos)
				{
					size_t D_Letter = line.find('T');
					double OFFSET = std::stod(line.substr(D_Letter + 4));
					std::get<4>(CS_STATUS) = 1;
					std::get<3>(CS_VARS) = OFFSET;
				}
			}
			if (std::get<0>(CS_STATUS) == 1 && std::get<1>(CS_STATUS) == 1 && std::get<2>(CS_STATUS) == 1 && std::get<3>(CS_STATUS) == 1 &&
						std::get<4>(CS_STATUS) == 1)
			{
				_GLOBAL_LOG_.SendInfo("Executing Charge Scan with THR = " + std::to_string((int)std::get<0>(CS_VARS)) + "e and TS = " + 
							std::to_string(std::get<1>(CS_VARS)) + "ns");
				ToTChargeScan(_GLOBAL_LOG_, std::get<0>(CS_VARS), std::get<1>(CS_VARS), std::get<2>(CS_VARS), std::get<3>(CS_VARS));
			}
		}
	}
	return 0;
}